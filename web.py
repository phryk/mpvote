from functools import wraps
from mpvote import mpvote
from werkzeug.datastructures import MultiDict
from flask import Flask, g, session, request, redirect, url_for, render_template, flash
from flask.ext.wtf import Form, TextField, RadioField, HiddenField, SubmitField

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['server'] = None
app.config['sitename'] = 'mpvote'
app.config['separator'] = '|'
app.secret_key = "foobar"
client = None



def title(title):

    def decorator(func):

        @wraps(func)
        def f(*args, **kwargs):
            title_set(title)
            return func(*args, **kwargs)
        
        return f
    return decorator


def content(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        content_set(f(*args, **kwargs))
        return render()

    return wrapper



class ctext(object):

    template = 'text.jinja'

    def __init__(self, value):
        self.value = value


class cform(ctext):
    
    template = 'form.jinja'



class IdentificationForm(Form):

    identity = TextField('Identification (name)')
    submit = SubmitField('Identify')


class VoteForm(Form):
    
    rating = RadioField('Rating', choices = [(x, x) for x in range(1,6)])
    file = HiddenField('File')
    submit = SubmitField('Rate')



def title_set(value):

    g.page['title'] = value
    g.page['head_title'] = ('%s %s %s' % (app.config['sitename'], app.config['separator'], value))


def content_set(value):
    
    g.page['content'] = value


def render(page = None):

    if page is None:
        page = g.page

    if type(page['content']).__name__ == 'BaseResponse':
        return page['content'] # make sure redirects work

    return render_template('index.jinja', page=page)



@app.before_request
def request_init():

    g.page = {}



@app.route('/')
@content
def vote():

    if not session.has_key('identity'):
        return redirect(url_for('identify'))

    status = client.status()

    if status['state'] == 'play':
        song = client.currentsong()
        data = MultiDict([('file', song['file'])])

        data['rating'] = client.getvote(session['identity'])

        title_set('Playing %s - %s' % (song['artist'], song['title']))
        return cform(VoteForm(data))

    return ctext('Not playing')


@app.route('/', methods=['POST'])
def vote_submit():

    data = request.form

    s = client.find('file', data['file'])

    if len(s):
        song = s[0]
    else:
        song = None

    client.vote(session['identity'], data['rating'], song)

    flash('%s - voted %s' % (song['title'], data['rating']))
    return redirect(url_for('vote'))



@app.route('/identify')
@title('Identify yourself')
@content
def identify():

    return cform(IdentificationForm())
    

@app.route('/identify', methods=['POST'])
def identify_submit():

    data = request.form

    identity = data['identity']
    if not len(identity):
        flash('Identity must be at least one char long.')
        return redirect(url_for('identify'))

    session['identity'] = identity
    return redirect(url_for('vote'))


if __name__ == '__main__':

    client = mpvote()
    client.connect(app.config['server'])
    app.run()
