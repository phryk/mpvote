#!/usr/bin/env python

"""
Usage:
    mpvote [options] vote [<rating>]

Options:
  -h --help             Show this screen.
  -s --server <server>  Specify the server ([<password>@]<host>[:<port>] notation), the $MPD_HOST and $MPD_PORT environment variables are used as fallback.
"""

from sys import exit
from os import environ
from getpass import getuser
from docopt import docopt
from mpd import MPDClient

args = None

class mpvote(MPDClient):

    def getvote(self, identifier, song = None):

            if song is None:

                song = self.currentsong()

                vid = 'vote:%s' % (identifier)

                try:
                    sticker = self.sticker_get('song', song['file'], vid)
                    rating = int(sticker[sticker.find('=')+1:])
                except Exception as e: #probably no sticker set
                    print "zero: ", vid, e
                    rating = 0
                
                return rating


    def vote(self, identifier, rating = 5, song = None):

            #status = self.status()
            status = {'state': 'stopped'}

            if song is None:

                if status['state'] == 'stop' and __name__ == '__main__':

                    print "Not playing, quitting."
                    exit(1) #TODO: only when running as __main__

                song = self.currentsong()

            vid = 'vote:%s' % (identifier)

            print "currentrating: ", self.getvote(identifier)

            r = self.sticker_set('song', song['file'], vid, rating)


            return {
                'status': status,
                'message': ("Voted for %s - %s!" % (song['artist'], song['title']))
            }
            


    def parse_host(self, s):

        server = {
            'password': None,
            'host': None,
            'port': None
        }

        if s.find('@') > 0:
          server['password'] = s[0:s.find('@')]
          s = s[s.find('@') + 1:]


        if s.find(':') > 0:
          server['host'] = s[0:s.find(':')]
          server['port'] = s[s.find(':') + 1:]

        else:
          server['host'] = s
          server['port'] = 6600

        return server


    def connect(self, s = None):

        if s is None:

            if args is not None and args['--server']:
                s = args['--server']

            elif environ.has_key('MPD_HOST'):
                s = environ['MPD_HOST']
                s = s+':'+environ['MPD_PORT'] if environ.get('MPD_PORT') else s

            else:
                s = 'localhost'

        server = self.parse_host(s)

        r = super(mpvote, self).connect(server['host'], server['port'])

        if server['password']:
            self.password(server['password'])

        return r


def db_query(query, args=(), one=False):
    cur = db.execute(query, args)
    rv= [dict((cur.description[idx][0], value)
        for idx, value in enumerate(row)) for row in cur.fetchall()]

    db.commit()
    return (rv[0] if rv else None) if one else rv



if __name__ == '__main__':

    args = docopt(__doc__)

    identifier = getuser()

    client = mpvote()
    client.connect()
    
    if args['vote']:
        rating = args['<rating>'] if args['<rating>'] else 5
        r = client.vote(identifier, rating)
        print r['message']
        exit(0)
       

